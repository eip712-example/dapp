import React, { useState } from 'react'
import abi from '../abi/MyEIP712abi.json'

const verifyingContract = "0xF13c292848a70ffca6743dDE6997BAA1513B7007"

function Signature({ web3, aliceAddress }) {
  const [signature, setSignature] = useState('')
  const [claim, setClaim] = useState()
  const [verified, setVerified] = useState('')

  const signWithMetamask = async () => {
    setSignature('')
    const chainId = await web3.eth.getChainId()

    const name = 'DApp that uses claims';
    const version = '1';

    const Claim = [
      { name: 'nonce', type: 'uint256'},
      { name: 'from', type: 'address'},
      { name: 'to', type: 'address'},
      { name: 'content', type: 'ClaimContent'}
    ]
    
    const ClaimContent = [
      { name: 'text', type: 'string'},
      { name: 'amount', type: 'uint256'},
    ]
  
    const EIP712Domain = [
      { name: 'name', type: 'string' },
      { name: 'version', type: 'string' },
      { name: 'chainId', type: 'uint256' },
      { name: 'verifyingContract', type: 'address' },
    ]

    const bob = aliceAddress
  
    const aliceClaim = {
      nonce: 1,
      from: aliceAddress,
      to: bob,
      content: {
        text: 'Alice pays 15 to Bob',
        amount: 15
      }
    }

    setClaim(aliceClaim)
  
    const domainData = {
      name: name,
      version: version,
      chainId: chainId,
      verifyingContract: verifyingContract
    }
  
    const data = {
      types: {
        EIP712Domain,
        Claim,
        ClaimContent
      },
      domain: domainData,
      primaryType: 'Claim',
      message: aliceClaim,
    }

    const aliceSignature = await web3.currentProvider.request({
      method: 'eth_signTypedData_v4',
      params: [aliceAddress, JSON.stringify(data)],
      from: aliceAddress,
    })

    setSignature(aliceSignature)
  }
  
  const verifySignature = async () => {
    const MyEIP712 = new web3.eth.Contract(abi, verifyingContract)
    const check = await MyEIP712.methods.verify(signature, aliceAddress, claim).call()
    if (check) {
      setVerified('Your signature is verified by smart contract!')
    } else {
      setVerified('Your signature is not verified by smart contract! :(')
    }
  }

  return (
    <>
      <div className='message'>
        <h2>Message to sign</h2>
        <p>
          nonce: <b>1</b><br/>
          from: <b>{aliceAddress}</b><br/>
          to: <b>{aliceAddress}</b><br/><br/>
          content: <br/>
            text: <b>Alice pays 15 to Bob</b><br/>
            amount: <b>15</b><br/>
        </p>
      </div>
      <button onClick={signWithMetamask}>
        Sign with Metamask
      </button>

      {signature && <div className='message'>
        <h2>Signature</h2>
        <p>{signature}</p>
        <button onClick={verifySignature}>
          Verify signature
        </button>
        <br/>
          <small>Verifying contract:<br/>
            <a 
              target='_blank' rel='noreferrer'
              href={`https://kovan.etherscan.io/address/${verifyingContract}`}
            >
              {verifyingContract}
            </a>
          </small>
        <p>{verified}</p>
      </div>}
    </>
  )
}

export default Signature
