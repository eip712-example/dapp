import React, { useEffect, useState } from 'react'
import Web3 from 'web3'
import './App.css'
import Signature from './components/Signature'

function App() {
  const [web3, setWeb3] = useState()
  const [aliceAddress, setAliceAddress] = useState()
 
  const connectToMetamask = async () => {
    if (window.ethereum) {
      const web3Provider = window.ethereum
      const account = await getAccount(web3Provider)
      return account
    } else {
      console.error("Can't connect to MetaMask!")
      return false
    }
  }

  const getAccount = async (web3Provider) => {
    const web3internal = new Web3(web3Provider)
    setWeb3(web3internal)
    try {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' })
        if (accounts && accounts[0]) {
            setAliceAddress(accounts[0])
        }
        const address = accounts[0]
        return address
    } catch (error) {
        console.log('User denied account access...', error)
        console.warn('Please allow access to your Web3 wallet.')
        return false
    }
  }

  useEffect(() => {
      connectToMetamask()
  }, [])

  return (
    <div className="App">
      {aliceAddress && 
        <Signature
          web3={web3}
          aliceAddress={aliceAddress}
        />
      }
    </div>
  )
}

export default App
